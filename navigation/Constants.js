export default {
    HOME: 'Home',
    POST:'Post',
    MY_NETWORK: 'My Network',
    SIGN_IN: 'Sign In',
    SIGN_UP: 'Sign Up',
    LANDING: 'Landing',
    DASHBOARD: 'Dashboard',
    EDIT_PROFILE: 'Edit Profile',
    PROFILE: 'Profile'
}
