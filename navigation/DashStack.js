import * as React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import LandingScreen from "../screens/LandingScreen";
import Constants from "./Constants";
import LogInScreen from "../screens/LogInScreen";
import SignUpScreen from "../screens/SignUpScreen";
import PostScreen from "../screens/PostScreen";
import EditProfileScreen from "../screens/EditProfileScreen";
import HomeScreen from "../screens/HomeScreen";
import ProfileScreen from "../screens/ProfileScreen";
import DashBoard from "../screens/DashBoard";


const AppAuthStack = createStackNavigator();

/*
React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
        let userToken;

        try {
            userToken = await getAsyncStringData('userToken');
        } catch (e) {
            // Restoring token failed
        }

        // After restoring token, we may need to validate it in production apps

        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    };
\
    bootstrapAsync().then(r => {});
}, []);
*/


export function NavigationStackScreens({navigation}) {
    return (
        <AppAuthStack.Navigator>
            <AppAuthStack.Screen name={Constants.DASHBOARD} component={DashBoard} options={{
                headerShown: false,
                animationEnabled: false
            }}/>

            <AppAuthStack.Screen name={Constants.LANDING} component={LandingScreen} options={{headerShown: false}}/>
            <AppAuthStack.Screen name={Constants.SIGN_IN} component={LogInScreen} options={{headerShown: false}}/>
            <AppAuthStack.Screen name={Constants.SIGN_UP} component={SignUpScreen} options={{headerShown: false}}/>

            <AppAuthStack.Screen name={Constants.EDIT_PROFILE} component={EditProfileScreen} options={{headerShown: true}}/>
            <AppAuthStack.Screen name={Constants.PROFILE} component={ProfileScreen} options={{headerShown: true}}/>
            <AppAuthStack.Screen name={Constants.POST} component={PostScreen} options={{headerShown: true}}/>
            <AppAuthStack.Screen name={Constants.HOME} component={HomeScreen} options={{headerShown: true}}/>


        </AppAuthStack.Navigator>
    )
}

export function DashStack({navigation}) {
    return (

        <AppAuthStack.Navigator screenOptions={{ presentation: 'modal' }}>
            {/*Navigation push animation stack*/}
            <AppAuthStack.Screen
                name="DashStack"
                component={NavigationStackScreens}
                options={{headerShown: false}}
            />

        </AppAuthStack.Navigator>
    );

}
