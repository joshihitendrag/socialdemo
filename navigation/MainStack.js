import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from "../screens/HomeScreen";
import MyNetwork from "../screens/MyNetwork";
import PostScreen from "../screens/PostScreen";
import Constants from "./Constants";


const HomeStack = createStackNavigator();
export function HomeStackScreen({navigation}) {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name={Constants.HOME} component={HomeScreen}/>
        </HomeStack.Navigator>
    );
}


const PostStack = createStackNavigator();
export function PostStackScreen({navigation}) {
    return (
        <PostStack.Navigator>
            <PostStack.Screen name={Constants.POST} component={PostScreen}/>
        </PostStack.Navigator>
    );
}

const MyNetworkStack = createStackNavigator();
export function MyNetworkStackScreen() {
    return (
        <MyNetworkStack.Navigator>
            <MyNetworkStack.Screen name={Constants.MY_NETWORK} component={MyNetwork}/>
        </MyNetworkStack.Navigator>
    );
}

