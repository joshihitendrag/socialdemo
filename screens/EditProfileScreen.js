import React from 'react';
import {StyleSheet, View, Text} from "react-native";

const EditProfileScreen = () => {

    return(<View>
        <Text  style={styles.textStyle}>Edit Profile</Text>
    </View>)
};

const styles = StyleSheet.create({

    textStyle:{
        fontSize: 12,
        color:'red'
    }

});

export default EditProfileScreen;
