import React from 'react';
import {StyleSheet, View, Text} from "react-native";

const LogInScreen = () => {

    return(<View>
        <Text  style={styles.textStyle}>LogIn</Text>
    </View>)
};

const styles = StyleSheet.create({

    textStyle:{
        fontSize: 12,
        color:'red'
    }

});

export default LogInScreen;
