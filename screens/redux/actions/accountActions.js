const IS_LOGGED_IN = 'IS_LOGGED_IN';
const CHANGE_LANG = "CHANGE_LANG";

export const accountActions = {
    IS_LOGGED_IN,
    CHANGE_LANG,
};
