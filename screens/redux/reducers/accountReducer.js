import {actions} from './../actions/index';
import en from "../../localisation/en.js";

const initialState = {
    lang: en,
    langId: 'en'
};

export function accountReducer(state = initialState, action) {
    switch (action.type) {
        case actions.account.IS_LOGGED_IN:
            return Object.assign({}, {...state, loggedIn: true});
       case actions.account.CHANGE_LANG:
            let language = {}
            if (action.payload.langId === 'en') {
                language = en
                //moment.locale('en-gb');
            } else {
                language = en; // add other language here
            }
            return Object.assign(
                {},
                {
                    ...state,
                    langId:action.payload.langId ? action.payload.langId : state.langId,
                    lang: language
                },
            );
        default:
            return state;
    }
}
