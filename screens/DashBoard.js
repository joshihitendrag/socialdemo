import React from 'react';
import {StyleSheet, View, Text} from "react-native";

const DashBoard = () => {

    return(<View style={{
        flex:1,
        backgroundColor:'blue'
    }}>
        <Text  style={styles.textStyle}>DashBoard</Text>
    </View>)
};

const styles = StyleSheet.create({

    textStyle:{
        fontSize: 12,
        color:'red'
    }

});

export default DashBoard;
