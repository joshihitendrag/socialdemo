import React from 'react';
import {StyleSheet, View, Text} from "react-native";

const HomeScreen = () => {

    return(<View>
        <Text  style={styles.textStyle}>Home</Text>
    </View>)
};

const styles = StyleSheet.create({

    textStyle:{
        fontSize: 12,
        color:'red'
    }

});

export default HomeScreen;
