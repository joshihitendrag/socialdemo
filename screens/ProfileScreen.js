import React from 'react';
import {StyleSheet, View, Text} from "react-native";

const ProfileScreen = () => {

    return(<View>
        <Text  style={styles.textStyle}>Profile</Text>
    </View>)
};

const styles = StyleSheet.create({

    textStyle:{
        fontSize: 12,
        color:'red'
    }

});

export default ProfileScreen;
