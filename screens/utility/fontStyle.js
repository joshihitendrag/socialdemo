export default {
    'AvenirHeavy': 'Avenir-Heavy',
    'AvenirMedium': 'Avenir-Medium',
    'AvenirBlack': 'Avenir-Black',
    'AvenirLight': 'Avenir-Light',
    'Cubic': 'Cubic',
    'SFProTextMedium': 'SFProText-Medium',
    'SFProTextRegular': 'SFProText-Regular'
};
