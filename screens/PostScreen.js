import React from 'react';
import {StyleSheet, View, Text} from "react-native";

const PostScreen = () => {

    return(<View>
        <Text  style={styles.textStyle}>Post</Text>
    </View>)
};

const styles = StyleSheet.create({

    textStyle:{
        fontSize: 12,
        color:'red'
    }

});

export default PostScreen;
