import React from 'react';
import {StyleSheet, View, Text} from "react-native";

const MyNetwork = () => {

    return(<View>
        <Text  style={styles.textStyle}>MyNetwork</Text>
    </View>)
};

const styles = StyleSheet.create({

    textStyle:{
        fontSize: 12,
        color:'red'
    }

});

export default MyNetwork;
