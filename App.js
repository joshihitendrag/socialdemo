/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {SafeAreaView, StatusBar, StyleSheet, useColorScheme, View,} from 'react-native';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';

import {Colors,} from 'react-native/Libraries/NewAppScreen';
import {DashStack} from "./navigation/DashStack";
import {Provider} from "react-redux";
import configureStore from "./screens/redux/store/configureStore";

const store = configureStore(window.__State__);

const App = () => {

    const isDarkMode = useColorScheme() === 'dark';
    const [isLoading, setLoading] = useState(false)
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };

    const renderRoot = () => {
        return <DashStack/>
    }

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: 'white'
            }}
        >

          {/*  <Spinner
                color={"#6d6c71"}
                textContent={'Logging you in , please wait...'}
                textStyle={{
                    color: 'white',
                    fontSize: 18,
                    fontFamily: fontStyle.AvenirBlack,
                }}
                visible={isLoading}
            />*/}
                <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'}/>

                <Provider store={store}>
                    <NavigationContainer>
                        <View style={[backgroundStyle, {
                            flex: 1,
                        }]}>
                            {renderRoot()}
                        </View>
                    </NavigationContainer>
                </Provider>

        </View>
    );
};

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    highlight: {
        fontWeight: '700',
    },
});

export default App;
